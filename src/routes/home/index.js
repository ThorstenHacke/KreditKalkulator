import { h } from 'preact';
import { useState } from 'preact/hooks'
import style from './style.css';




function AktuelleRate(Kredit, Monat){
    const keys = Object.keys(Kredit.Raten).filter(key => key <= Monat);
    const RatenKey = keys[keys.length-1];
    return Kredit.Raten[RatenKey];
}

function Calculate(OrgKredite, Zinzsteigung, SondertilgungProJahr){
const Kredite = JSON.parse(JSON.stringify(OrgKredite));
let Monat = 1;
const exitMonate = [];
const monate = [];
while(Kredite.filter(k => k.Kreditbetrag > 0).length > 0 && Monat < 1000){
    let toRemove = [];
    for(const Kredit of Kredite.filter(k => k.Kreditbetrag > 0)){
        Kredit.Kreditbetrag = BerechneKredit(Kredit.Kreditbetrag, AktuelleRate(Kredit, Monat)+Kredit.SonderRate, Kredit.Zinssatz);
        if(Kredit.Kreditbetrag < 0 ){
            toRemove.push(Kredit);
            exitMonate.push({Monat, Kredit})
        }
    }
  
    for(const Kredit of Kredite.filter(k => k.Kreditbetrag < 0)){
        if(Kredite.length > 0){
            const anteil = 1/Kredite.length;
            for(const KreditChange of Kredite.filter(k => k.Kreditbetrag > 0)){
                KreditChange.Kreditbetrag += Kredit.Kreditbetrag*anteil;
                KreditChange.SonderRate += AktuelleRate(Kredit, Monat)*anteil;

            }
            Kredit.Kreditbetrag = 0;
        }
    }
    if(Monat%12 == 0){
        for(const KreditChange of Kredite.filter(k => k.Kreditbetrag > 0)){
            KreditChange.Kreditbetrag -= (SondertilgungProJahr/Kredite.filter(k => k.Kreditbetrag > 0).length)
        }
    }
    for(const Kredit of Kredite.filter(k => k.Kreditbetrag > 0)){
        if(Monat == Kredit.Zinzbindung){
            Kredit.Zinssatz+= Zinzsteigung;
        }
    }
    const monatsKredite = JSON.parse(JSON.stringify(Kredite));
    monate.push({
        Monat,
        Kredite: monatsKredite
    })
    Monat++;
}
return monate
}
function BerechneKredit(Kreditbetrag, Rate, Zinssatz){
    Kreditbetrag = (Kreditbetrag - Rate) + (Kreditbetrag * (Zinssatz/100/12));
    return Kreditbetrag
}
function getYearchMonth(month){
    const years = (Math.floor(month/12))
    const monthsLeft = month-(years*12);
    return years + " : "+monthsLeft;
}
const Home = () => {

    const tempKredite = [{
        Id: 1,
        Kreditbetrag : 100000,
        Raten : {
            0:250,
            24:350
        },
        Zinssatz : 0.75,
        Zinzbindung: 10*12,
        SonderRate:0
    },
    {
        Id: 2,
        Kreditbetrag : 200000,
        Raten : {
            0:700,
            24:850
        },
        Zinssatz : 1.5,
        Zinzbindung: 20*12,
        SonderRate:0
    },
    {
        Id: 3,
        Kreditbetrag : 100000,
        Raten : {
            0:250, 
            24: 300},
        Zinssatz : 1,
        Zinzbindung: 15*12,
        SonderRate:0
    }
    ]
    const [OrgKredite, setOrgKredite] = useState(tempKredite);
    const [Zinzsteigung, setZinzsteigung] = useState(2);
    const [SondertilgungProJahr, setSondertilgungProJahr] = useState(3000);
    const monate = Calculate(OrgKredite, Zinzsteigung, SondertilgungProJahr)
    const handleInputZinzsteigung = (e) => {
        console.log(e);
        setZinzsteigung(Number.parseInt(e.target.value));
      }
      const handleInputSondertilgungProJahr = (e) => {
        console.log(e);
        setSondertilgungProJahr(Number.parseInt(e.target.value));
      }
      const handleKredite = (e) => {
        console.log(e);
        setOrgKredite(JSON.parse(e.target.value));
      }
	return (<div class={style.home}>
        {/* <h1>Raten: {OrgKredite.reduce((p,a) => p+a.Rate, 0)}</h1> */}
        <span>Kredite: </span>
        <textarea id="Kredite" value={JSON.stringify(OrgKredite)} rows="12" cols="120" onInput={handleKredite}></textarea ><br></br>
        <span>Zinzsteigung nach Zinzbindung: </span>
        <input id="Zinzsteigung" value={Zinzsteigung} type="number" onInput={handleInputZinzsteigung}></input><br></br>
        <span>Sondertilgung pro Jahr: </span>
        <input id="SondertilgungProJahr" value={SondertilgungProJahr} type="number" onInput={handleInputSondertilgungProJahr}></input>
        <div class={style.monate}>
        <><div>Jahr : Monat</div>{Object.keys(OrgKredite).map(k => {return <div>Kredit: {OrgKredite[k].Id}</div>})}<div>Rate Gesamt</div><div>Rest Gesamt</div></>
        <><div>{ getYearchMonth(0)}</div>{Object.keys(OrgKredite).map(k => {return <div>{OrgKredite[k].Kreditbetrag.toFixed(2)}</div>})}<div>{OrgKredite.reduce((p, a)=> p+ AktuelleRate(a,0),0).toFixed(2)}</div><div>{OrgKredite.reduce((p, a)=> p+ a.Kreditbetrag,0).toFixed(2)}</div></>
        {monate.map(m => {
            const res = <><div>{ getYearchMonth(m.Monat)}</div>{Object.keys(m.Kredite).map(k => {return <div>{m.Kredite[k].Kreditbetrag.toFixed(2)}</div>})}<div>{OrgKredite.reduce((p, a)=> p+ AktuelleRate(a,m.Monat),0).toFixed(2)}</div><div>{m.Kredite.reduce((p, a)=> p+ a.Kreditbetrag,0).toFixed(2)}</div></>
            return res;
        })}
        </div>
	</div>)
};

export default Home;
